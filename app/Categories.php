<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
// instance of Categories class will refer to categories table in database
class Categories extends Model {
    //restricts columns from modifying
    protected $guarded = [];

    protected $fillable = [
        'title','slug'
    ];
    // returns the instance of the user who is author of that post
    public function author()
    {
        return $this->belongsTo('App\User','author_id');
    }
    // user has many feeds
    public function feed()
    {
        return $this->hasMany('App\Feed','category_id');
    }
}
