@extends('layouts.app')
@section('title')
Edit Feed
@endsection
@section('content')
<form method="post" action='{{ url("/update-feed") }}'>
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="feed_id" value="{{ $feed->id }}{{ old('category_id') }}">
  <div class="form-group">
    <input required="required"  placeholder="Enter title here" type="text" name = "title" class="form-control" value="@if(!old('title')){{$feed  ->title}}@endif{{ old('title') }}"/>
  </div>
   <div class="form-group">
    <input required="required" readonly  placeholder="Enter title here" type="text" name = "url" class="form-control" value="@if(!old('url')){{$feed  ->url}}@endif{{ old('uel') }}"/>
  </div>
    <div class="form-group">
        <select class="form-control" name="category_id" id="category_id">
               @foreach( $categories as $category )
                <option @if($feed->category_id == $category->id) selected @endif value="{{ $category->id }}">{{ $category->title }}</option>
               @endforeach
        </select>
    </div>
  <input type="submit" name='publish' class="btn btn-success" value = "Update"/>
  <a href="{{  url('update-feed-content/'.$feed->id.'?_token='.csrf_token()) }}" class="btn btn-success">Update Feed Content</a>
  <a href="{{  url('delete-feed/'.$feed->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Delete</a>
</form>

 <div class="col-md-12 " style="margin-top: 15px    ">
            <div class="panel panel-default">
                <div class="list-group " style="margin-top: 5px" >
                    @foreach( $feedContents as $content)
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-md-9">
                                 {{str_limit($content->title,100) }}
                                </div>
                                <div class="col-md-3">
                                 {{$content->created_at }}
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
@endsection