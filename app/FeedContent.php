<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedContent extends Model
{
    //restricts columns from modifying
    protected $guarded = [];


    protected $table = 'feed_content';

    // user has many feeds
    public function feeds()
    {
        return $this->belongsTo('App\Feed','feed_id');
    }
}
