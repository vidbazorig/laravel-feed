<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','FeedController@home');
Route::get('/home',['as' => 'home', 'uses' => 'FeedController@home']);

Auth::routes();
// Categories
Route::group(['middleware' => ['auth']], function() {

    Route::get('categories', 'CategoryController@index');
    Route::get('new-category', 'CategoryController@create');
    Route::post('new-category', 'CategoryController@store');
    Route::get('edit-category/{slug}', 'CategoryController@edit');
    Route::post('update', 'CategoryController@update');
    Route::get('delete-category/{id}', 'CategoryController@destroy');

//Feeds

    Route::get('feeds', 'FeedController@index');
    Route::get('new-feed', 'FeedController@create');
    Route::post('new-feed', 'FeedController@store');
    Route::get('edit-feed/{id}', 'FeedController@edit');
    Route::get('update-feed-content/{id}', 'FeedController@updateContent');
    Route::post('update-feed', 'FeedController@update');
    Route::get('delete-feed/{id}', 'FeedController@destroy');

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/edit-profile', 'Auth\EditprofileController@editprofile');
    Route::post('/edit-profile', 'Auth\EditprofileController@saveeditprofile');
});
Route::get('/{slug}',['as' => 'category', 'uses' => 'FeedController@show'])->where('slug', '[A-Za-z0-9-_]+');