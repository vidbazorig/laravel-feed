@extends('layouts.app')
@section('title')
    Add New Category
@endsection
@section('content')
<form action="/new-category" method="post">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <input required="required" value="{{ old('title') }}" placeholder="Enter category title here" type="text" name = "title" class="form-control" />
  </div>
  <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>

</form>
@endsection