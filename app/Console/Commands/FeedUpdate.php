<?php

namespace App\Console\Commands;

use App\Feed;
use App\FeedContent;
use Illuminate\Console\Command;

class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Feeds Content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feeds = Feed::orderBy('created_at', 'desc')->get();
        $bar = $this->output->createProgressBar(count($feeds));
        foreach ($feeds as $feed) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $feed->url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_COOKIE, 'AspxAutoDetectCookieSupport=1');
            $output = curl_exec($ch);
            curl_close($ch);
            $xml = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA);

            if ($xml->channel) {

                foreach ($xml->channel->item as $item) {
                    $feedContent = new FeedContent;
                    $feedContent->feed_id = $feed->id;
                    $feedContent->provider = $xml->channel->link;
                    $dublicate = false;
                    foreach ($feed->feedContents as $content) {
                        if (str_slug($content->title) == str_slug($item->title)) {
                            $dublicate = true;
                        }
                    }
                    $feedContent->title = $item->title;
                    $feedContent->url = $item->link;
                    $feedContent->content = $item->description;
                    if (!$dublicate) {
                        $feedContent->save();
                    }
                }
            } elseif ($xml->entry) {
                foreach ($xml->entry as $item) {
                    $feedContent = new FeedContent;
                    $feedContent->feed_id = $feed->id;
                    $feedContent->title = $item->title;
                    $dublicate = false;
                    foreach ($feed->feedContents as $content) {
                        if (str_slug($content->title) == str_slug($item->title)) {
                            $dublicate = true;
                        }
                    }
                    foreach ($xml->link as $val) {
                        $feedContent->provider = $val->attributes()->{'href'};
                    }
                    $feedContent->url = $item->link->attributes()->{'href'};
                    $feedContent->content = $item->content;
                    if (!$dublicate) {
                        $feedContent->save();
                    }

                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('');
        $this->info('Feeds updated');

    }
}
