@extends('layouts.app')
@section('title')
Edit Categorys
@endsection
@section('content')

@if ( !$categories->count() )
There is no categories.     <a href="new-category" class="btn btn-success">Create Category</a>
@else

    <div class="list-group " style="margin-top: 5px" >
        @foreach( $categories as $category )
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-10">
                          <h3 style="margin:0" >{{ $category->title }}</h3>
                    </div>
                    <div class="col-md-2 ">
                        @if(!Auth::guest() && ($category->author_id == Auth::user()->id ))
                            <a class="btn" href="{{ url('edit-category/'.$category->slug)}}">Edit Post</a>
                        @endif
                     </div>
                </div>
            </div>
        @endforeach
    </div>

@endif
@endsection