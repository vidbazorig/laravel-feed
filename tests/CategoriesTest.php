<?php

use App\Categories;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoriesTest extends TestCase
{

     use DatabaseMigrations;

    public function testCategoriesCanBeCreated()
    {
        $user = factory(App\User::class)->create();
        $category = $user->categories()->create([
           'title'=> 'Cars',
           'slug'=> 'cars',
        ]);
        $created_category = Categories::find(1);

        $this->assertEquals($created_category->title,'Cars');
        $this->assertEquals($created_category->slug,'cars');
    }

    public function testCategoriesCanBeEdited()
    {
        $user = factory(App\User::class)->create();
        $category = $user->categories()->create([
           'title'=> 'Cars',
           'slug'=> 'cars',
        ]);

        $category->title = 'Tvs';
        $category->save();
        $created_category = Categories::find(1);

        $this->assertEquals($created_category->title,'Tvs');
        $this->assertEquals($created_category->slug,'cars');
    }
    public function testCategoriesCanBeDeleted()
    {
        $user = factory(App\User::class)->create();
        $category = $user->categories()->create([
           'title'=> 'Cars',
           'slug'=> 'cars',
        ]);


        $category->delete();
        $created_category = Categories::find(1);

        $this->notSeeInDatabase('categories',['id'=>'1','title'=>'Cars']);

    }
}
