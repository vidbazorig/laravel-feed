@extends('layouts.app')
@section('title')
               Add New feed
@endsection
@section('content')
            <div class="col-md-12" >
                @if ( !$categories->count() )
                    There are no categories pleas create one to post feed
                    <a href="new-category" class="btn btn-success">Create Category</a>
                @else
                    <form action="/new-feed" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <input required="required" value="{{ old('title') }}" placeholder="Enter feed title here" type="text" name = "title"class="form-control" />
                      </div>
                       <div class="form-group">
                        <input value="{{ old('url') }}" placeholder="Enter feed url here" type="text" name = "url"class="form-control" />
                      </div>
                      <div class="form-group">
                      <select class="form-control" name="category_id" id="category_id">
                         @foreach( $categories as $category )
                          <option value="{{ $category->id }}">{{ $category->title }}</option>
                         @endforeach
                      </select>
                    </div>
                      <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
                    </form>
                    @endif
            </div>
@endsection