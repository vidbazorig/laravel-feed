


$(document).on("click", ".open-Dialog", function () {

    var title = $(this).data('title');
    var link = $(this).data('link');
    var contentDesciption = $(this).data('content');
    $(".modal-title").html( title );
    $("#myModal #innerMDContent").html(contentDesciption);
    $("#myModal #provider_link").attr("href", link);
});