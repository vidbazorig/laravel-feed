# Laravel Rss Feed Aplication
Created By  Vdimantas Bazevicius 

## Official Documentation

To start install composer with [composer install} command.

Next This application  you need to Start Laravel with : [php artisan serve] command.

Create .env file and config it.

To test  [vendor/bin/phpunit] command.

Finally  start [php artisan migrate] command to create tables.

## Working With Application

To work with application you need  to create user first using [php artisan create:user] command.

User then can login  to his account, create new categories, feeds and update created feed in web browser.

To  update all  the feeds use [php artisan feed:update] command.

Enjoy your own Rss Feed Application.   
 