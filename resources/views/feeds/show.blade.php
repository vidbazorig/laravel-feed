@extends('layouts.app')
@section('title')
{{$title}} - {{$category->title}}
@endsection
@section('content')



        <div class="col-md-3 ">
        <h3>Categories</h3>
            <div class="panel panel-default">
               <ul class="">
                    @foreach( $categories as $value )
                       <li>
                           <a @if($category->id == $value->id) style="font-weight:bold;" @endif href="/{{$value->slug}}">{{$value->title}}</a>
                       </li>
                    @endforeach
               </ul>
            </div>
        </div>
        <div class="col-md-9 ">
            @if ( !$contents )
                There is no feeds  at The moments
            @else
                <div class="panel panel-default">
                    <div class="list-group " style="margin-top: 5px" >

                        @foreach( $contents as $feedContent )
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <a href="#"  data-toggle="modal" data-title="{{$feedContent->title}}"  data-link="{{$feedContent->url}}" data-content="{{$feedContent->content}}" data-title="{{$feedContent->title}}" data-target="#myModal" class="open-Dialog">{{ $feedContent->title }}</a>
                                    </div>
                                    <div class="col-md-4">
                                          <a  href="{{ $feedContent->provider }}" target="_blank" style="margin:0" >{{ $feedContent->provider }}</a>
                                    </div>
                                    <div class="col-md-3">
                                        <p style="margin:0" >{{ $feedContent->created_at }}</p>
                                    </div>

                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
           @endif
        </div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div id ="innerMDContent"></div>
      </div>
      <div class="modal-footer">
       <a href="" id="provider_link" target="_blank" class="btn btn-success">Read Article</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection