@extends('layouts.app')
@section('title')
Feeds
@endsection
@section('content')

@if ( !$feeds->count() )
There is no feeds . Login and write a creat a new feed now!!!
  <a href="{{  url('new-feed') }}" class="btn btn-success">Create Feed </a>
@else

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="list-group " style="margin-top: 5px" >
                    @foreach( $feeds as $feed )
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-md-8">
                                      <h3 style="margin:0" >{{ $feed->title }}</h3>
                                </div>
                                    @if(!Auth::guest() && ($feed->author_id == Auth::user()->id ))
                                <div class="col-md-2 ">
                                        <a class="btn btn-success" href="{{ url('edit-feed/'.$feed->id)}}">Edit</a>
                                 </div>
                                   <div class="col-md-2 ">
                                        <a class="btn btn-danger" href="{{ url('delete-feed/'.$feed->id)}}">Delete </a>
                                 </div>
                                    @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>



@endif
@endsection