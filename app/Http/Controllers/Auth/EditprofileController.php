<?php
/**
 * Created by PhpStorm.
 * User: vidbaz
 * Date: 16.11.21
 * Time: 17.48
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class EditprofileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editprofile()
    {
        $user = Auth::user();
        return view('auth.editProfile',["user" => $user]);
    }

    public function saveeditprofile(Request $input){

        $this->validate($input, [
            'name' => 'required|max:255',
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'min:6'
        ]);

        $user =Auth::user();
        $user->name = $input["name"];
        if ($input->has('password'))
            $user->password = bcrypt($input['password']);
        $user->save();

        return redirect('edit-profile');

    }

}