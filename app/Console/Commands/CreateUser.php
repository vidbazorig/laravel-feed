<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User as Users;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CreateUsers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('What is your usename?');
        $user = Users::where('name', $name)->first();
        if($user){
            $this->error('username is taken try again');
            return true;
        }
        $email = $this->ask('What is your email?');
        $user = Users::where('email', $email)->first();
        if($user){
            $this->error('email is taken try again');
            return true;
        }
        $password = $this->secret('What is your password?');

        if(!filter_var( $email, FILTER_VALIDATE_EMAIL )){
            $this->error('bad email address try again');
        }else{
            return Users::create([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password),
            ]);

        };
    }
}
