<?php

namespace App\Http\Controllers;

use App\Categories;
use App\User;
use Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;
use Illuminate\Http\Request;


class CategoryController extends Controller
{


    public function index()
    {

        $categories = Categories::orderBy('created_at', 'desc')->get();

        //page heading
        $title = 'Categories';

        return view('categories.index')->withCategories($categories)->withTitle($title);
    }

    public function create(Request $request)
    {
        // if user can post i.e. user is admin or author

        return view('categories.create');

    }

    public function store(CategoryFormRequest $request)
    {
        $category = new Categories();
        $category->title = $request->get('title');
        $category->slug = str_slug($category->title);
        $category->author_id = $request->user()->id;
        $duplicate = Categories::where('slug', $category->slug)->first();

        if ($duplicate) {

            return redirect('new-category/')->withErrors('Category  already exists.')->withInput();

        }

        $message = 'Category created successfully';

        $category->save();
        return redirect('categories')->withMessage($message);
    }

    public function edit(Request $request, $slug)
    {
        $category = Categories::where('slug', $slug)->first();
        if ($category && ($request->user()->id == $category->author_id))
            return view('categories.edit')->with('category', $category);
        return redirect('/categories')->withErrors('you have not sufficient permissions');
    }

    public function update(Request $request)
    {
        $category_id = $request->input('category_id');
        $category = Categories::find($category_id);
        if ($category && ($category->author_id == $request->user()->id)) {
            $title = $request->input('title');
            $slug = str_slug($title);

            $duplicate = Categories::where('slug', $slug)->first();
            if ($duplicate) {
                if ($duplicate->id != $category_id) {
                    return redirect('edit-category/' . $category->slug)->withErrors('Title already exists.')->withInput();
                } else {
                    $category->slug = $slug;
                }
            }
            $category->title = $title;
            $message = 'Category updated successfully';
            $landing = 'edit-category/' . $category->slug;

            $category->save();

            return redirect($landing)->withMessage($message);
        } else {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    public function destroy(Request $request, $id)
    {
        //
        $category = Categories::find($id);
        if ($category && ($category->author_id == $request->user()->id)) {
            $category->delete();
            $data['message'] = 'Category deleted Successfully';
        } else {
            $data['errors'] = 'Invalid Operation. You have not sufficient permissions';
        }
        return redirect('/categories')->with($data);
    }

}
