<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedsConntent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_content', function(Blueprint $table)
        {
            $table->increments('id');

            $table -> integer('feed_id') -> unsigned() -> default(0);
            $table->foreign('feed_id') ->references('id')->on('feed') ->onDelete('cascade');
            $table->string('title')->unique();
            $table->string('url')->unique();
            $table->string('provider');
            $table->text('content');


      $table->timestamps();
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_content');
    }
}
