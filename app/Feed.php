<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
// instance of Categories class will refer to categories table in database
class Feed extends Model {
    //restricts columns from modifying
    protected $guarded = [];

    protected $table = 'feed';

    protected $fillable = [
        'name', 'email', 'password',
    ];
    // returns the instance of the user who is author of that post
    public function author()
    {
        return $this->belongsTo('App\User','author_id');
    }
     //
    public function categories()
    {
        return $this->belongsTo('App\Categories','category_id');
    }
    // user has many feeds
    public function feedContents()
    {
        return $this->hasMany('App\FeedContent','feed_id');
    }
}
