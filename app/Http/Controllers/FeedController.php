<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Feed;
use App\FeedContent;
use App\User;
use Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeedFormRequest;
use Illuminate\Http\Request;


class FeedController extends Controller
{


    public function index()
    {
        $feeds = Feed::orderBy('created_at', 'desc')->get();
        $title = 'Feeds';
        return view('feeds.feeds')->withFeeds($feeds)->withTitle($title);
    }

    public function home()
    {
        $feeds = FeedContent::orderBy('created_at', 'desc')->limit(50)->get();
        $categories = Categories::orderBy('created_at', 'desc')->get();
        $title = 'Feeds';

        return view('feeds.home')->withFeeds($feeds)->withTitle($title)->withCategories($categories);
    }

    public function show($slug)
    {
        $category = Categories::where('slug', $slug)->orderBy('created_at', 'desc')->first();
        $categories = Categories::orderBy('created_at', 'desc')->get();
        if ($category) {
            $feeds = Feed::with('feedContents')->where('category_id', $category->id)->get();
            $feed = Feed::with('feedContents')->where('category_id', $category->id)->first();
            $feed_ids = array();
            foreach ($feeds as $val) {
                $feed_ids[] = $val->id;
            }
            if (isset($feeds)) {
                $feedContents = FeedContent::whereIn('feed_id', $feed_ids)->orderBy('created_at', 'desc')->get();

            } else {
                return redirect('/error/404');
            }
        } else {
            return redirect('/error/404');

        }
        $title = 'Feeds';

        return view('feeds.show')->withCategory($category)->withTitle($title)->withCategories($categories)->withContents($feedContents);
    }

    public function create(Request $request)
    {

        $categories = Categories::orderBy('created_at', 'desc')->get();
        return view('feeds.create')->withCategories($categories);

    }

    public function store(FeedFormRequest $request)
    {
        $feed = new Feed();
        $feed->title = $request->get('title');
        $feed->url = $request->get('url');
        $feed->category_id = $request->get('category_id');
        $feed->author_id = $request->user()->id;
        $duplicate = Feed::where('url', $feed->url)->first();
        if ($duplicate) {
            return redirect('new-feed')->withErrors('This feed url  already exists.')->withInput();
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed->url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_COOKIE, 'AspxAutoDetectCookieSupport=1');
        $output = curl_exec($ch);
        $conType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close($ch);
        var_dump($conType);

        if (preg_match('/html/',$conType) == false) {
            $xml = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA);
        }else{
            return redirect('/new-feed')->withErrors('your  feed url is not rss feed');
        }


        $feed->save();
        if ($xml->channel) {
            $feed->save();
            foreach ($xml->channel->item as $item) {
                $feedContent = new FeedContent;
                $feedContent->feed_id = $feed->id;
                $feedContent->provider = $xml->channel->link;
                $feedContent->title = $item->title;
                $feedContent->url = $item->link;
                $feedContent->content = $item->description;
                $feedContent->save();
            }
        } elseif ($xml->entry) {
            $feed->save();
            foreach ($xml->entry as $item) {

                $feedContent = new FeedContent;
                $feedContent->feed_id = $feed->id;
                $feedContent->title = $item->title;
                foreach ($xml->link as $val) {
                    $feedContent->provider = $val->attributes()->{'href'};
                }
                $feedContent->url = $item->link->attributes()->{'href'};
                $feedContent->content = $item->content;

                $feedContent->save();
            }
        }else{
            return redirect('/new-feed')->withErrors('your  feed url is nor rss feed');
        }

        $message = 'Feed created successfully';
        $landing = 'edit-feed/' . $feed->id;
        return redirect($landing)->withMessage($message);
    }

    public function edit(Request $request, $id)
    {
        $feed = Feed::with('feedContents')->find($id);
        $feedContents = $feed->feedContents()->orderBy('created_at', 'desc')->get();

        $categories = Categories::orderBy('created_at', 'desc')->get();
        if ($feed && ($request->user()->id == $feed->author_id))
            return view('feeds.edit')->with('feed', $feed)->with('categories', $categories)->with('feedContents', $feedContents);
        return redirect('/')->withErrors('you have not sufficient permissions');
    }

    public function updateContent(Request $request, $id)
    {

        $feed = Feed::with('feedContents')->where('id', $id)->first();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed->url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_COOKIE, 'AspxAutoDetectCookieSupport=1');
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA);

        // parse rss
        if ($xml->channel) {
            foreach ($xml->channel->item as $item) {
                $feedContent = new FeedContent;
                $feedContent->feed_id = $feed->id;
                $dublicate = false;
                foreach ($feed->feedContents as $content) {
                    if (str_slug($content->title) == str_slug($item->title)) {
                        $dublicate = true;
                    }
                }
                $feedContent->title = $item->title;
                $feedContent->url = $item->link;
                $feedContent->content = $item->description;
                if (!$dublicate) {
                    $feedContent->save();
                }
            }
        } elseif ($xml->entry) {
            foreach ($xml->entry as $item) {
                $feedContent = new FeedContent;
                $feedContent->feed_id = $feed->id;
                $feedContent->title = $item->title;
                $dublicate = false;
                foreach ($feed->feedContents as $content) {
                    if (str_slug($content->title) == str_slug($item->title)) {
                        $dublicate = true;
                    }
                }
                foreach ($xml->link as $val) {
                    $feedContent->provider = $val->attributes()->{'href'};
                }
                $feedContent->url = $item->link->attributes()->{'href'};
                $feedContent->content = $item->content;
                if (!$dublicate) {
                    $feedContent->save();
                }

            }
        }
        $landing = 'edit-feed/' . $feed->id;
        $message = 'Feed Content Updated';
        return redirect($landing)->withMessage($message);
    }

    public function update(Request $request)
    {
        $feed_id = $request->input('feed_id');
        $feed = Feed::find($feed_id);
        if ($feed && ($feed->author_id == $request->user()->id)) {
            $title = $request->input('title');


            $feed->title = $title;
            $feed->url = $request->input('url');;
            $feed->category_id = $request->input('category_id');
            $message = 'Feed edited successfully';
            $landing = 'edit-feed/' . $feed->id;

            $feed->save();

            return redirect($landing)->withMessage($message);
        } else {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    public function destroy(Request $request, $id)
    {
        $feed = Feed::find($id);
        if ($feed && ($feed->author_id == $request->user()->id)) {
            $feed->delete();
            $data['message'] = 'Feed deleted Successfully';
        } else {
            $data['errors'] = 'Invalid Operation. You have not sufficient permissions';
        }
        return redirect('/feeds')->with($data);
    }

}
